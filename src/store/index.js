import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function getRandArray (length) {
  let arr = []
  for (let i = 0; i < length; i++) {
    arr.push({
      string: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
      number: Math.random()
    })
  }
  return arr
}
function getList () {
  let arr = []
  for (let i = 1; i <= 1200; i++) {
    arr.push({
      id: i,
      value: i,
      randArr: [
        getRandArray(100), getRandArray(100), getRandArray(100)
      ]
    })
  }
  return arr
}

const SET_VALUE = 'SET_VALUE'
export const mutationNames = {
  SET_VALUE
}
export const storeConfig = {
  state: {
    list: getList()
  },
  mutations: {
    [SET_VALUE] (state, { value, i }) {
      // let newList = [...state.list]
      // newList[i].value = value
      // state.list = newList
      state.list[i].value = value
    }
  },
  actions: {
    setValue ({commit}, data) {
      commit(SET_VALUE, data)
    }
  },
  getters: {
    getList: state => state.list.map(item => {
      item['test'] = '111'
      return item
    })
  }
}
export default new Vuex.Store(storeConfig)
