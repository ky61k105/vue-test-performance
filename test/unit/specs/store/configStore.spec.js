import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import { cloneDeep } from 'lodash'
import { storeConfig } from '@/store'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Vuex must have correct state object ', () => {
  let store = new Vuex.Store(cloneDeep(storeConfig))
  beforeEach(() => {
    localStorage.clear()
  })
  describe('state.list ', () => {
    it('should be a array', () => {
      store.state.list.should.be.a('array')
    })

    it('should have langth == 1200', () => {
      store.state.list.length.should.equal(1200)
    })
  })
})
